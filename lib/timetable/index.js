const assert = require('assert')
const { r } = require('config')

const { EventEmitter } = require('events')

const makeEvent = (type, data) => ({
    type,
    data,
    occured_at: new Date()
})

class TimetableService extends EventEmitter {

    constructor({viewTable, timelineTable}) {
        super()
        this.view = viewTable
        this.timeline = timelineTable
        timelineTable.changes().toStream().on('data', ({new_val, old_val}) => {
            console.log(new_val)
            if(new_val.type === "slotCreated") {
                this.view.insert(Object.assign({}, {id: new_val.id}, new_val.data))
                .then(result => assert(result.inserted, 1))
                .catch(e => console.log(e))
            }

            if(new_val.type === "slotUpdated") {
                this.view.get(new_val.data.id).update(new_val.data)
                .then(result => console.log(result))
                .catch(e => console.log(e))
            }
        })
    }

    add(slot) {
        this.timeline.insert(makeEvent('slotCreated', slot))
        .then(result => {
            assert.equal(result.inserted, 1)

        })
        .catch(e => console.log(e))
        return true
    }

    remove(id) {
         this.timeline.insert(Object.assign(makeEvent('slotRemoved', {id})))
         .then(result => assert.equal(result.inserted, 1))
         .catch(error => console.log(error))
        // emit slotRemoved event
        return true
    }

    update(id, slot) {
        this.timeline.insert(makeEvent('slotUpdated', Object.assign({id}, slot)))
        .then(result => {
            assert.equal(result.inserted, 1)
        })
        .catch(e => console.log(e))
        return true
    }

    get(id) {
        return this.view.get(id);
    }

    index() {
        return this.view
    }
    replay() {
        // this.timeline.reduce()
        // populate current viewTable or updates it
    }

}


const timetableService = new TimetableService({
    viewTable: r.table('timetable'),
    timelineTable: r.table('timetable_timeline')
})

module.exports = timetableService