const awilix = require('awilix')
const timetableService = require('timetable')

const container = awilix.createContainer()

container.register({
    timetableService: awilix.asValue(timetableService)
})

module.exports = container