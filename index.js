const restify = require('restify');
const container = require('container')
const server = restify.createServer();
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.authorizationParser());
server.use(restify.plugins.dateParser());
server.use(restify.plugins.queryParser());
server.use(restify.plugins.gzipResponse());
server.use(restify.plugins.bodyParser());


const timetableService = container.resolve('timetableService')

server.post('/timetable', (req, res, next) => {
  timetableService.add(req.body)
  res.send('ok')
  return next();
})

server.get('/timetable', (req, res, next) => {
  timetableService.index()
  .then(result => res.send(result))
  .catch(error => next(error))
  return next()
})


server.get('/timetable/:id', (req, res, next) => {
  timetableService.get(req.params.id)
  .then(result => res.send(result))
  .catch(error => next(error))
  return next()
})

server.put('/timetable/:id', (req, res, next) => {
  timetableService.update(req.params.id, req.body)

  res.send('ok')

  return next()
})



server.listen(10000, function() {
  console.log('%s listening at %s', server.name, server.url);
});